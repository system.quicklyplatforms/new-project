const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const authRoutes = require('./src/routes/auth');
const productRoutes = require('./src/routes/products');

const app = express();
const port = 8080

const dbSrv = "mongodb+srv://bandana:bandana27@cluster0.t70vikb.mongodb.net/flutter_interview";
console.log("ss",dbSrv);
mongoose.set("strictQuery", false);
db = mongoose.connect(
    dbSrv,
    (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("Database connected");
        }
    }
);


app.use(bodyParser.json());

app.use('/auth', authRoutes);
app.use('/products', productRoutes);



app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
