const express = require('express');
const productController = require('../controlllers/productController');
const router = express.Router();

router.post('/add', productController.addProduct);
router.get('/get', productController.getProducts);
router.get('/getById/:id', productController.getProduct);
router.post('/update/:id', productController.editProduct);
router.post('/delete/:id', productController.deleteProduct);

module.exports = router;