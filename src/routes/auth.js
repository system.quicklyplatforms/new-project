const express = require('express');
const authController = require('../controlllers/authController');
const router = express.Router();


router.post('/login', authController.login);

module.exports = router;